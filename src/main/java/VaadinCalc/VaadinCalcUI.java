package VaadinCalc;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import javax.servlet.annotation.WebServlet;
import java.text.DecimalFormat;
import java.text.NumberFormat;

@SpringUI
@Theme("mytheme")
public class VaadinCalcUI extends UI {


    VerticalLayout layout; // основной макет

    private Label message = new Label(); // Label для вывода сообщений


    @Override
    protected void init(VaadinRequest vaadinRequest) {

        // поочереди запускаем методы отображения макетов с содержимым

        setupLayout(); // конфигурирование основного макета

        showHeader(); // отображаем шапку

        showCalcPanel(); // отображаем панель с основными компонентами

    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = VaadinCalcUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }


    private void setupLayout() {     // конфигурирование основного макета
        layout = new VerticalLayout();
        layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        layout.setMargin(true);
        layout.setSpacing(true);

        setContent(layout);
    }


    private void showHeader() { // шапка

        Label header = new Label("Калькулятор");
        header.addStyleName(ValoTheme.LABEL_H2);
        layout.addComponent(header);
    }

    private double calculateNum(double a, double b) { // метод вычисления

        // можно не обрабатывать и не пробрасывать ArithmeticException,
        // т.к. double все равно его не выкинет, т.к. работает с делением на 0

        return a/b;
    }


    private void showCalcPanel() {

        // создаем панель
        Panel panelCalc = new Panel("Калькулятор");
        panelCalc.setWidth(450, Unit.PIXELS);
        panelCalc.setHeight(370, Unit.PIXELS);
        panelCalc.setIcon(VaadinIcons.CALC);
        panelCalc.addStyleName(ValoTheme.PANEL_WELL);

        // панель содержит calcLayout
        VerticalLayout calcLayout = new VerticalLayout();
        calcLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        calcLayout.setWidth(100, Unit.PERCENTAGE);
        calcLayout.setHeight(100, Unit.PERCENTAGE);
        calcLayout.setMargin(true);
        calcLayout.setSpacing(true);

        // задаем параметры Label для вывода сообщений
        message.setCaption("");
        message.setValue("");
        message.setVisible(false);

        // создаем делимое
        TextField a_number = new TextField();
        a_number.setCaption("Делимое A");
        a_number.setPlaceholder("Введите делимое A");
        a_number.setMaxLength(15); // количество вводимых сиволов ограничено

        // создаем делитель
        TextField b_number = new TextField();
        b_number.setCaption("Делитель B");
        b_number.setPlaceholder("Введите делитель B");
        b_number.setMaxLength(15); // количество вводимых сиволов ограничено

        // создаем частное
        TextField c_number = new TextField();
        c_number.setCaption("Результат C");
        c_number.setPlaceholder("Пока ничего...");
        c_number.setReadOnly(true); // поле только для чтения

        // кнопка Вычислить "="
        Button buttonCalculate = new Button("=");
        buttonCalculate.setStyleName(ValoTheme.BUTTON_PRIMARY);

        // обработчик изменения значения делимого
        a_number.addValueChangeListener(event -> {

                   if (!a_number.getValue().isEmpty()) {
                       if (!checkMatchDigits(a_number.getValue())) showMatchError();
                    else message.setVisible(false);
                   }
                }
        );

        // обработчик изменения значения делителя
        b_number.addValueChangeListener(event -> {

            if (!b_number.getValue().isEmpty()) {
                if (!checkMatchDigits(b_number.getValue())) showMatchError();
                else message.setVisible(false);
            }
                }
        );


        // обработчик нажатия на кнопку "="
        buttonCalculate.addClickListener
                (event ->
                {
                    c_number.clear(); // очищаем поле с результатом

                    // проверка на ввод недопустимых символов и корректность ввода в целом
                    if (checkMatchDigits(a_number.getValue()) && checkMatchDigits(b_number.getValue()))
                    {
                        double a = Double.valueOf(a_number.getValue().replace(",",".")); // работаем с точками как разделяющим символом
                        double b = Double.valueOf(b_number.getValue().replace(",","."));

                        // проверка на 0 в делителе
                    if (b!=0) // хотя double и работает с 0 в делителе, но общепринято, что на 0 делить нельзя
                    {
                        NumberFormat doubleformat = new DecimalFormat("#.####"); // приводим вывод к данному формату

                        c_number.setValue(String.valueOf(doubleformat.format(calculateNum(a,b))).replace(",",".")); // производим вычисление
                    }
                    else {

                        showZeroError(); // иначе выводим ошибку
                    }

                    } else showMatchError(); // иначе выводим ошибку

                });


        // добавляем на макет компоненты
        calcLayout.addComponents(message, a_number, b_number, c_number, buttonCalculate);

        panelCalc.setContent(calcLayout);
        layout.addComponent(panelCalc);
    }

    private void showZeroError(){ // вывод ошибки деления на 0

        message.setValue("Ошибка! На ноль делить нельзя!");
        message.setStyleName(ValoTheme.LABEL_FAILURE);
        message.setVisible(true);
    }

    private void showMatchError(){ // вывод ошибки некорректного ввода

        message.setValue("Ошибка! Проверьте корректность ввода");
        message.setStyleName(ValoTheme.LABEL_FAILURE);
        message.setVisible(true);
    }

    private boolean checkMatchDigits(String str) { // проверка на ввод недопустимых символов

        if (str.replace(",",".").matches("(-|\\+)?\\d*\\.?,?\\d+")) return true; // проверяем на соответствие регулярному выражению
        else return false;
    }

}
